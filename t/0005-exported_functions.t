#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use Data::Dumper;
use lib "$Bin/../lib/";
use lib "$Bin/lib/";

use Test::More;
use Test::MockModule;
use Mail::SpamAssassin::Plugin::CountryFilter;
use Test::CountryFilter::Helper;

my $sa = Mail::SpamAssassin->new();

my $plugin = Mail::SpamAssassin::Plugin::CountryFilter->new($sa);

my $opts = [
    {
        key   => 'blacklist_relay_country_codes',
        value => 'FR,ES',
    },
    {
        key   => 'whitelist_relay_country_codes',
        value => 'GB,SE',
    },
    {
        key   => 'blacklist_source_country_codes',
        value => 'US,IT',
    },
    {
        key   => 'whitelist_source_country_codes',
        value => 'NL,BE',
    },
];

foreach my $opt (@$opts) {
    ok( $plugin->parse_config($opt), "parse_config() on $opt->{ key }" );
}

## Blacklist relay country check
is(
    $plugin->blacklist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_PT_01, $IPV4_FR_01
            )
        }
    ),
    1,
'blacklist_relay_country_check() reports true correctly when offending IPv4 FR address is found'
);

is(
    $plugin->blacklist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_PT_01, $IPV6_FR_01
            )
        }
    ),
    1,
'blacklist_relay_country_check() reports true correctly when offending IPv6 FR address is found'
);

is(
    $plugin->blacklist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_PT_01, $IPV4_ES_01
            )
        }
    ),
    1,
'blacklist_relay_country_check() reports true correctly when offending IPv4 ES address is found'
);

is(
    $plugin->blacklist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_PT_01, $IPV6_ES_01
            )
        }
    ),
    1,
'blacklist_relay_country_check() reports true correctly when offending IPv6 ES address is found'
);

is(
    $plugin->blacklist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_FR_01, $IPV4_PT_01
            )
        }
    ),
    0,
'blacklist_relay_country_check() retuens false correctly when IPv4 FR address exists but is not a relay'
);

is(
    $plugin->blacklist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_FR_01, $IPV6_PT_01
            )
        }
    ),
    0,
'blacklist_relay_country_check() reports false correctly when IPv6 FR address exists but is not a relay'
);

# Whitelist relay country check
is(
    $plugin->whitelist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_PT_01, $IPV4_GB_01
            )
        }
    ),
    1,
'whitelist_relay_country_check() reports true correctly when IPv4 GB address is found'
);

is(
    $plugin->whitelist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_PT_01, $IPV6_GB_01
            )
        }
    ),
    1,
'whitelist_relay_country_check() reports true correctly when IPv6 GB address is found'
);

is(
    $plugin->whitelist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_PT_01, $IPV4_SE_01
            )
        }
    ),
    1,
'whitelist_relay_country_check() reports true correctly when IPv4 SE address is found'
);

is(
    $plugin->whitelist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_PT_01, $IPV6_SE_01
            )
        }
    ),
    1,
'whitelist_relay_country_check() reports true correctly when IPv6 SE address is found'
);

is(
    $plugin->whitelist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_GB_01, $IPV4_PT_01
            )
        }
    ),
    0,
'whitelist_relay_country_check() retuens false correctly when IPv4 GB address exists but is not a relay'
);

is(
    $plugin->whitelist_relay_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_GB_01, $IPV6_PT_01
            )
        }
    ),
    0,
'whitelist_relay_country_check() reports false correctly when IPv6 GB address exists but is not a relay'
);

# Blacklist source country check
is(
    $plugin->blacklist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_US_01, $IPV4_PT_01
            )
        }
    ),
    1,
'blacklist_source_country_check() reports true correctly when offending IPv4 US address is found'
);

is(
    $plugin->blacklist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_US_01, $IPV6_PT_01
            )
        }
    ),
    1,
'blacklist_source_country_check() reports true correctly when offending IPv6 US address is found'
);

is(
    $plugin->blacklist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_IT_01, $IPV4_PT_01
            )
        }
    ),
    1,
'blacklist_source_country_check() reports true correctly when offending IPv4 IT address is found'
);

is(
    $plugin->blacklist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_IT_01, $IPV6_PT_01
            )
        }
    ),
    1,
'blacklist_source_country_check() reports true correctly when offending IPv6 IT address is found'
);

is(
    $plugin->blacklist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_PT_01, $IPV4_US_01
            )
        }
    ),
    0,
'blacklist_source_country_check() retuens false correctly when IPv4 US address exists but is not the source'
);

is(
    $plugin->blacklist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_PT_01, $IPV6_US_01
            )
        }
    ),
    0,
'blacklist_source_country_check() reports false correctly when IPv6 US address exists but is not the source'
);

# Whitelist source country check
is(
    $plugin->whitelist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_NL_01, $IPV4_PT_01
            )
        }
    ),
    1,
'whitelist_source_country_check() reports true correctly when offending IPv4 NL address is found'
);

is(
    $plugin->whitelist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_NL_01, $IPV6_PT_01
            )
        }
    ),
    1,
'whitelist_source_country_check() reports true correctly when offending IPv6 NL address is found'
);

is(
    $plugin->whitelist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_BE_01, $IPV4_PT_01
            )
        }
    ),
    1,
'whitelist_source_country_check() reports true correctly when offending IPv4 BE address is found'
);

is(
    $plugin->whitelist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_BE_01, $IPV6_PT_01
            )
        }
    ),
    1,
'whitelist_source_country_check() reports true correctly when offending IPv6 BE address is found'
);

is(
    $plugin->whitelist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV4_LOCAL_01, $IPV4_PT_01, $IPV4_NL_01
            )
        }
    ),
    0,
'whitelist_source_country_check() retuens false correctly when IPv4 NL address exists but is not the source'
);

is(
    $plugin->whitelist_source_country_check(
        {
            relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                $IPV6_LOCAL_01, $IPV6_PT_01, $IPV6_NL_01
            )
        }
    ),
    0,
'whitelist_source_country_check() reports false correctly when IPv6 NL address exists but is not the source'
);

done_testing();


#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";

use Mail::SpamAssassin;
use Test::More;

my $sa = Mail::SpamAssassin->new();

require_ok('Mail::SpamAssassin::Plugin::CountryFilter');

my $plugin = Mail::SpamAssassin::Plugin::CountryFilter->new($sa);
isa_ok( $plugin, 'Mail::SpamAssassin::Plugin' );

my $conf = $sa->{conf};

my @rule_functions = qw/
  blacklist_relay_country_check
  whitelist_relay_country_check
  blacklist_source_country_check
  whitelist_source_country_check
  /;

foreach my $rule_function (@rule_functions) {
    ok(
        ref( $conf->{eval_plugins}{$rule_function} ) eq
          'Mail::SpamAssassin::Plugin::CountryFilter',
        "$rule_function registered with SA"
    );
}

done_testing();

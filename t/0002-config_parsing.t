#!/usr/bin/env perl

use strict;
use warnings;
use Data::Dumper;
use FindBin qw($Bin);
use lib "$Bin/../lib/";

use Mail::SpamAssassin;
use Test::More;
use Mail::SpamAssassin::Plugin::CountryFilter;

$Data::Dumper::Terse = 1;
my $sa     = Mail::SpamAssassin->new();
my $plugin = Mail::SpamAssassin::Plugin::CountryFilter->new($sa);
my $conf   = $sa->{conf};

# Clean and split codes
my $test_codes = [
    [ "lc",           { LC => 1 } ],
    [ "lC,Cl",        { LC => 1, CL => 1 } ],
    [ "a.f,i:g,:,as", { AF => 1, IG => 1, AS => 1 } ],
    [ "af,,,,,,,,ar", { AF => 1, AR => 1 } ],
    [ "",             {} ]
];

foreach my $test_code (@$test_codes) {
    my ( $input, $expected ) = @$test_code;
    is_deeply(
        Mail::SpamAssassin::Plugin::CountryFilter::_clean_and_split_codes(
            $input),
        $expected,
        "_clean_and_split_codes() - $input"
    );
}

my $config_sets = [
    {
        opts => {
            key         => 'blacklist_relay_country_codes',
            value       => 'GB,FR',
            line        => 1,
            conf        => $conf,
            user_config => 0,
        },
        expected_return_value => 1,
        expected              => { GB => 1, FR => 1 },
    },
    {
        opts => {
            key         => 'whitelist_relay_country_codes',
            value       => 'FR,DE',
            line        => 1,
            conf        => $conf,
            user_config => 0,
        },
        expected_return_value => 1,
        expected              => { FR => 1, DE => 1 },
    },
    {
        opts => {
            key         => 'blacklist_source_country_codes',
            value       => 'DE,ES',
            line        => 1,
            conf        => $conf,
            user_config => 0,
        },
        expected_return_value => 1,
        expected              => { DE => 1, ES => 1 },
    },
    {
        opts => {
            key         => 'whitelist_source_country_codes',
            value       => 'ES,IT',
            line        => 1,
            conf        => $conf,
            user_config => 0,
        },
        expected_return_value => 1,
        expected              => { ES => 1, IT => 1 },
    },
    {
        opts => {
            key         => 'geoip_ipv4_database',
            value       => '/tmp/geo_ip_db_v4.dat',
            line        => 1,
            conf        => $conf,
            user_config => 0,
        },
        expected_return_value => 1,
        expected              => '/tmp/geo_ip_db_v4.dat',
    },
    {
        opts => {
            key         => 'geoip_ipv6_database',
            value       => '/tmp/geo_ip_db_v6.dat',
            line        => 1,
            conf        => $conf,
            user_config => 0,
        },
        expected_return_value => 1,
        expected              => '/tmp/geo_ip_db_v6.dat',
    },
    {
        opts => {
            key         => 'blacklist_relay_country_codes',
            value       => 'DE,SE',
            line        => 1,
            conf        => $conf,
            user_config => 1,
        },
        expected_return_value => 1,
        expected              => { DE => 1, SE => 1 },
    },
    {
        opts => {
            key         => 'invalid_key',
            value       => 'DE,SE',
            line        => 1,
            conf        => $conf,
            user_config => 0,
        },
        expected_return_value => 0,
        expected              => undef,
    },
];

foreach my $config_set (@$config_sets) {
    my $opts                  = $config_set->{opts};
    my $expected              = $config_set->{expected};
    my $expected_return_value = $config_set->{expected_return_value};

    is( $plugin->parse_config($opts),
        $expected_return_value,
        "'$opts->{ key }' parse_config() returns true" );
    my $printable_value =
      defined( $opts->{value} )
      ? "'$opts->{ value }'"
      : "undef";

    my $printable_expected = 'undef';
    if ( defined($expected) ) {
        if ( ref($expected) ) {
            $printable_expected = Dumper($expected);
        }
        else {
            $printable_expected = $expected;
        }
    }

    is_deeply(
        $conf->{country_filter}{ $opts->{key} },
        $expected,
        "Config '$opts->{ key }' = "
          . $printable_value
          . " parsed correctly as '$printable_expected'"
    );
}

done_testing();

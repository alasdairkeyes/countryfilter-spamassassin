#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use Data::Dumper;
use lib "$Bin/../lib/";
use lib "$Bin/lib/";

use Test::More;
use Test::MockModule;
use Mail::SpamAssassin::Plugin::CountryFilter;
use Test::CountryFilter::Helper;

my $sa = Mail::SpamAssassin->new();

my $plugin = Mail::SpamAssassin::Plugin::CountryFilter->new($sa);

is( $plugin->_ip_to_country_code($IPV4_US_01),
    'US', "_ip_to_country_code() with $IPV4_US_01 resolves to US" );

is( $plugin->_ip_to_country_code($IPV6_US_01),
    'US', "_ip_to_country_code() with $IPV6_US_01 resolves to US" );

is( $plugin->_ip_to_country_code(''),
    undef, '_ip_to_country_code() with empty string returns undef' );

is( $plugin->_ip_to_country_code(),
    undef, '_ip_to_country_code() with no IP returns undef' );

is( $plugin->_ip_to_country_code('-'),
    undef, '_ip_to_country_code() with non IP returns undef' );

is_deeply(
    [
        $plugin->_check_ip_country_codes(
            { US => 1 },
            $IPV4_US_01, $IPV4_US_02
        )
    ],
    [ "$IPV4_US_01|US", "$IPV4_US_02|US" ],
'_check_ip_country_codes() for US based IPv4 addresses return correct Country code response'
);

is_deeply(
    [
        $plugin->_check_ip_country_codes(
            { US => 1 },
            $IPV6_US_01, $IPV6_US_02
        )
    ],
    [ "$IPV6_US_01|US", "$IPV6_US_02|US" ],
'_check_ip_country_codes() Check for US based IPv6 addresses return correct Country code response'
);

is_deeply(
    [
        $plugin->_check_ip_country_codes(
            { US => 1 },
            $IPV4_US_01, $IPV6_US_02
        )
    ],
    [ "172.67.71.247|US", "$IPV6_US_02|US" ],
'_check_ip_country_codes() Check for US based IPv6/IPv4 mixed addresses return correct Country code response'
);

is_deeply(
    [
        $plugin->_check_ip_country_codes(
            { JP => 1 },
            $IPV4_US_01, $IPV6_US_02
        )
    ],
    [],
'_check_ip_country_codes() Check for JP based IPv6/IPv4 from US mixed addresses return correct Country code response'
);

is_deeply(
    [
        $plugin->_check_ip_country_codes(
            { FR => 1 },
            $IPV4_US_01, $IPV6_US_02, $IPV4_FR_01, $IPV6_FR_01
        )
    ],
    [ "$IPV4_FR_01|FR", "$IPV6_FR_01|FR" ],
'_check_ip_country_codes() Check for FR based IPv6/IPv4 from FR mixed addresses from different countries return correct Country code response'
);

is_deeply( [ $plugin->_check_ip_country_codes( { US => 1 } ) ],
    [], '_check_ip_country_codes() with no IP produces expected result' );

done_testing();

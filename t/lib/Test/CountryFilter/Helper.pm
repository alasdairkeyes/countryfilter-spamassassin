package Test::CountryFilter::Helper;

use strict;
use warnings;
use base 'Exporter';

# All Country IPs should be within the GeoIP database
# US IPs taken from nic.com
our $IPV4_US_01 = '172.67.71.247';
our $IPV4_US_02 = '104.26.13.222';
our $IPV6_US_01 = '2606:4700:20::ac43:47f7';
our $IPV6_US_02 = '2606:4700:20::681a:cde';

# FR IPs taken from nic.fr
our $IPV4_FR_01 = '192.134.5.37';
our $IPV6_FR_01 = '2001:67c:2218:302::51:231';

# IT IPs taken from nic.it
our $IPV4_IT_01 = '192.12.192.40';
our $IPV6_IT_01 = '2a00:d40:1:1:192:12:192:40';

# SE IPs taken from nic.se
our $IPV4_SE_01 = '159.253.30.207';
our $IPV6_SE_01 = '2a02:750:7:3305::2d4';

# BE IPs taken from nic.be
our $IPV4_BE_01 = '5.134.5.17';
our $IPV6_BE_01 = '2a00:1c98:1000:1213:0:2:cc81:42c4';

# NL IPs taken from nic.nl
our $IPV4_NL_01 = '212.114.120.64';
our $IPV6_NL_01 = '2001:7b8:62b:1:212:114:120:64';

# GB IPs taken from nic.uk nameservers
our $IPV4_GB_01 = '213.248.216.1';
our $IPV6_GB_01 = '2a01:618:400::1';

# ES IPs taken from nic.es
our $IPV4_ES_01 = '194.69.254.54';
our $IPV6_ES_01 = '2001:67c:21cc:313::15';

# PT IPs taken from nic.pt
our $IPV4_PT_01 = '185.39.208.69';
our $IPV6_PT_01 = '2a04:6d80:0:1::5';

# The doc and local IPs should not be in the GeoIP database
# Reserved IPs for documentation usage
our $IPV4_DOC_01 = '203.0.113.1';
our $IPV4_DOC_02 = '203.0.113.2';
our $IPV6_DOC_01 = '2001:db8::::::1';
our $IPV6_DOC_02 = '2001:db8::::::2';

# Localnet IPs
our $IPV4_LOCAL_01 = '127.0.0.1';
our $IPV6_LOCAL_01 = 'fe80::ffff:ffff:ffff:fffe';

our @EXPORT = qw/
  $IPV4_US_01
  $IPV4_US_02
  $IPV6_US_01
  $IPV6_US_02

  $IPV4_FR_01
  $IPV6_FR_01

  $IPV4_IT_01
  $IPV6_IT_01

  $IPV4_SE_01
  $IPV6_SE_01

  $IPV4_BE_01
  $IPV6_BE_01

  $IPV4_NL_01
  $IPV6_NL_01

  $IPV4_GB_01
  $IPV6_GB_01

  $IPV4_ES_01
  $IPV6_ES_01

  $IPV4_PT_01
  $IPV6_PT_01

  $IPV4_DOC_01
  $IPV4_DOC_02
  $IPV6_DOC_01
  $IPV6_DOC_02

  $IPV4_LOCAL_01
  $IPV6_LOCAL_01
  /;

# This mimics the structure under the relays_untrusted key of the Mail::SpamAssassin::PerMsgStatus object
sub generate_pms {
    my ( $local_src_ip, $src_ip, $relay_ip ) = @_;
    return [
        {
            'helo'      => 'host2.example.org',
            'lc_by'     => 'receivinghost.example.com',
            'as_string' =>
"[ ip=$relay_ip rdns=host2.example.org helo=host2.example.org by=receivinghost.example.com ident= envfrom=bounce-8856433-1545578009\@host2.example.org intl=0 id=1gb5VA-0004CC-Qx auth= msa=0 ]",
            'lc_rdns'    => 'host2.example.org',
            'ip'         => $relay_ip,
            'by'         => 'receivinghost.example.com',
            'rdns'       => 'host2.example.org',
            'auth'       => '',
            'msa'        => 0,
            'lc_helo'    => 'host2.example.org',
            'envfrom'    => 'bounce-8856433-1545578009@host2.example.org',
            'id'         => '1gb5VA-0004CC-Qx',
            'ip_private' => '',
            'ident'      => '',
            'internal'   => 0
        },
        {
            'ident'      => '',
            'internal'   => 0,
            'msa'        => 0,
            'lc_helo'    => 'host3.example.org',
            'envfrom'    => '',
            'ip_private' => '',
            'id'         => '6D2FE23A3BE',
            'by'         => 'host2.example.org',
            'rdns'       => 'bulk3',
            'auth'       => '',
            'helo'       => 'host3.example.org',
            'as_string'  =>
"[ ip=$src_ip rdns=bulk3 helo=host3.example.org by=host2.example.org ident= envfrom= intl=0 id=6D2FE23A3BE auth= msa=0 ]",
            'lc_by'   => 'host2.example.org',
            'lc_rdns' => 'bulk3',
            'ip'      => $src_ip
        },
        {
            'internal'   => 0,
            'ident'      => '',
            'envfrom'    => '',
            'id'         => '493E4AA0140',
            'ip_private' => 1,
            'lc_helo'    => "!$local_src_ip!",
            'msa'        => 0,
            'auth'       => '',
            'by'         => 'host3.example.org',
            'rdns'       => 'localhost',
            'as_string'  =>
"[ ip=$local_src_ip rdns=localhost helo=!$local_src_ip! by=host3.example.org ident= envfrom= intl=0 id=493E4AA0140 auth= msa=0 ]",
            'lc_rdns' => 'localhost',
            'lc_by'   => 'host3.example.org',
            'ip'      => $local_src_ip,
            'helo'    => "!$local_src_ip!"
        }
    ];
}

1;

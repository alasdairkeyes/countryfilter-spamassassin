#!/usr/bin/env perl

use strict;
use warnings;
use FindBin qw($Bin);
use lib "$Bin/../lib/";
use lib "$Bin/lib/";

use Test::More;
use Test::MockModule;
use Mail::SpamAssassin::Plugin::CountryFilter;
use Test::CountryFilter::Helper;

# Test IPv4 Addresses
is_deeply(
    [
        Mail::SpamAssassin::Plugin::CountryFilter::_get_all_public_ips(
            {
                relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                    $IPV4_LOCAL_01, $IPV4_DOC_01, $IPV4_DOC_02
                )
            }
        )
    ],
    [ '203.0.113.2', '203.0.113.1' ],
    '_get_all_public_ips() Public IPv4 IPs extracted'
);

is_deeply(
    [
        Mail::SpamAssassin::Plugin::CountryFilter::_get_relay_public_ips(
            {
                relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                    $IPV4_LOCAL_01, $IPV4_DOC_01, $IPV4_DOC_02
                )
            }
        )
    ],
    ['203.0.113.2'],
    '_get_relay_public_ips() Public IPv4 relays extracted'
);

is_deeply(
    [
        Mail::SpamAssassin::Plugin::CountryFilter::_get_source_public_ip(
            {
                relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                    $IPV4_LOCAL_01, $IPV4_DOC_01, $IPV4_DOC_02
                )
            }
        )
    ],
    ['203.0.113.1'],
    '_get_source_public_ip() Public IPv4 source extracted'
);

# Test IPv6 Addresses
is_deeply(
    [
        Mail::SpamAssassin::Plugin::CountryFilter::_get_all_public_ips(
            {
                relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                    $IPV6_LOCAL_01, $IPV6_DOC_01, $IPV6_DOC_02
                )
            }
        )
    ],
    [ '2001:db8::::::2', '2001:db8::::::1' ],
    '_get_all_public_ips() Public IPs extracted'
);

is_deeply(
    [
        Mail::SpamAssassin::Plugin::CountryFilter::_get_relay_public_ips(
            {
                relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                    $IPV6_LOCAL_01, $IPV6_DOC_01, $IPV6_DOC_02
                )
            }
        )
    ],
    ['2001:db8::::::2'],
    '_get_relay_public_ips() Public relays extracted'
);

is_deeply(
    [
        Mail::SpamAssassin::Plugin::CountryFilter::_get_source_public_ip(
            {
                relays_untrusted => Test::CountryFilter::Helper::generate_pms(
                    $IPV6_LOCAL_01, $IPV6_DOC_01, $IPV6_DOC_02
                )
            }
        )
    ],
    ['2001:db8::::::1'],
    '_get_source_public_ip() Public source extracted'
);

# Test empty IP Addresses
is_deeply( [ Mail::SpamAssassin::Plugin::CountryFilter::_get_all_public_ips() ],
    [], '_get_all_public_ips() performs OK with no input' );

is_deeply(
    [ Mail::SpamAssassin::Plugin::CountryFilter::_get_relay_public_ips() ],
    [], '_get_relay_public_ips() performs OK with no input' );

is_deeply(
    [ Mail::SpamAssassin::Plugin::CountryFilter::_get_source_public_ip() ],
    [undef], '_get_source_public_ip() performs OK with no input' );

{
    my $dbg_tests = [
        {
            input    => [ 'test message', 'one' ],
            expected => 'country_filter: test message one',
        },
        {
            input    => [],
            expected => 'country_filter: -'
        }
    ];
    foreach my $dbg_test (@$dbg_tests) {
        my @input    = @{ $dbg_test->{input} };
        my $expected = $dbg_test->{expected};

        my $sa_plugin_dbg_called   = 0;
        my $sa_plugin_dbg_argument = '';
        my $sa_plugin_mock =
          Test::MockModule->new('Mail::SpamAssassin::Plugin');
        $sa_plugin_mock->mock( 'dbg',
            sub { $sa_plugin_dbg_argument = shift; $sa_plugin_dbg_called = 1 }
        );

        ok( Mail::SpamAssassin::Plugin::CountryFilter::dbg(@input),
            'dbg() function called without error' );
        is( $sa_plugin_dbg_called, 1,
            'Mail::SpamAssassin::Plugin::dbg() called when dbg() is called.' );
        is( $sa_plugin_dbg_argument, $expected,
'dbg() passes correctly formatted message to Mail::SpamAssassin::Plugin::dbg()'
        );
    }
}

done_testing();

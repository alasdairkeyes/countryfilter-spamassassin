#!/bin/bash

apt update

apt upgrade -y

# Install requirements for the code
apt install -y \
    spamassassin \
    libgeo-ip-perl \
    libnet-ip-perl \
    libnetaddr-ip-perl

# Install requirements for testing
apt install -y \
    libtest-exception-perl \
    libtest-mockobject-perl \
    libtest-mockmodule-perl \
    libarray-compare-perl \
    libdevel-cover-perl \
    make \
    libpod-coverage-perl \
    perltidy

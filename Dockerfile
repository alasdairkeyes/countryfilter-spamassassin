FROM debian:bookworm
WORKDIR /app

VOLUME ["/app"]

COPY ./ /app/

RUN ci/system_setup.sh

CMD ["ci/run_tests.sh"]
